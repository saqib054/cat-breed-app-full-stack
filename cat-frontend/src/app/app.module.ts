import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BreedListComponent } from './components/breed-list/breed-list.component';
import { BreedComponent } from './components/breed/breed.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatCardModule,
	MatInputModule,
	MatButtonModule,
	MatIconModule,
	MatProgressSpinnerModule,
	MatSnackBarModule,
	MatDialogModule,
	MatTableModule,
  MatTooltipModule,
  MatChipsModule,
  MatPaginatorModule   } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    BreedListComponent,
    BreedComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatIconModule,
    MatChipsModule,
    MatPaginatorModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
