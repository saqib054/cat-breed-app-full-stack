export class Breed{
    id?: number;
    name: string;
    description: string;
    temperament?: string;
    origin?: string;
}