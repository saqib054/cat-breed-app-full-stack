import { Component, OnInit } from '@angular/core';
import { BreedServiceService } from '../breed-service.service';
import { Observable, throwError, pipe } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Component({
  selector: 'app-breed-list',
  templateUrl: './breed-list.component.html',
  styleUrls: ['./breed-list.component.css']
})
export class BreedListComponent implements OnInit {

  breedsData$: Observable<any>;
  error;
  length;
  pageIndex = 0;
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  constructor(public breedServiceService: BreedServiceService) { }

  ngOnInit() {
		this.getBreeds();
  }
  
  getBreeds() {
    this.breedsData$ = this.breedServiceService.getBreeds(this.pageIndex, this.pageSize);
  }
  
  searchBreed(name) {
    if(!name) {
      this.getBreeds();
      return;
    };
    this.breedsData$ = this.breedServiceService.searchBreed(name).pipe(
      catchError(err => {
        this.error = err.error;
        return throwError(err);
      })
    );
    this.error = null;
  }

  ngAfterViewInit() {
    this.getBreeds();
  }

  pageOptions(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getBreeds();
  }

}
