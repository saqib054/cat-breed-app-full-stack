import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Breed } from './breed.model';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BreedServiceService {
	apiUrl = 'api/';

  constructor( private http: HttpClient) { }

	// get all breeds
	getBreeds(pageIndex, size) {
		const params = {
			pageIndex: pageIndex + 1, size
		}
		return this.http
			.get(this.apiUrl + 'breed', { params });
	}

	// get only one breed
	getBreed(breedId: string) {
		return this.http.get(this.apiUrl+ 'breed/' + breedId);
	}

	// add breed
	addBreed(formValue: Breed) {
		return this.http
			.post(this.apiUrl + 'breed', formValue);
	}

	// update breed
	updateBreed(breedId: string, formValue: Breed) {
		return this.http
			.put(this.apiUrl + 'updateCustomer/' + breedId, formValue);
	}

	// remove breed
	removeBreed(id: string) {
		return this.http
			.delete(this.apiUrl + 'customerProfile/' + id);
	}

	// remove breed
	searchBreed(name: string): Observable<any> {
		return this.http
			.get(this.apiUrl + 'breed/search/' + name);
	}
}
