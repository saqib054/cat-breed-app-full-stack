# CAT - API
Nodejs, Expressjs and MongoDB based api to create, read and delete Cat breeds, search breed by name

**Access APP Deployed To Heroku**  
`https://secret-reaches-44162.herokuapp.com/`

**Access API Deployed To Heroku**  
`https://blooming-everglades-27685.herokuapp.com/api/breed`

## Installation

**Note**  
You need to have [Node.js](http://nodejs.org/) installed on your machine.  
You need to have MongoDB key to connect to a database.

## 1) Clone the repository, install node packages



`git clone https://bitbucket.org/saqib054/cat-api.git`  
`cd cat-api`  
`npm install`  

## Starting server

Setup .env file with followind credentials
  
`
    mongoURI: 'mongodb-Key'
`

`Run npm start`  
Your app should now be running on [localhost:5000](http://localhost:5000/). 

Open postaman and try accessing  
https://localhost:5000/api/breed  
https://localhost:5000/api/breed/:id  

## License
MIT