const express = require('express');
const router = express.Router();
const Breed = require('../models/breed');
const { ErrorHandler } = require('../helpers/error');

// retrieve all breeds
// @route GET api/breeds
// @access Public
router.get('/breed', async (req, res, next) => {
	const pageIndex = parseInt(req.query.pageIndex) || 1;
	const size = parseInt(req.query.size) || 5;
	try {
		const totalCount = await Breed.countDocuments();
		const totalPages = Math.ceil(totalCount / size);
		const breeds = await Breed.find().skip(size * (pageIndex - 1)).limit(size);
		if (!breeds) {
			throw new ErrorHandler(404, `Breeds does not exists`);
		}
		res.status(200);
		res.json({breeds, totalCount});
	}
	catch (error) {
		next(error)
	}
});

// Get breed by id
// @route GET api/breed/id
// @access Public
router.get('/breed/:id', async (req, res, next) => {
	try {
		const breed = await Breed.findById(req.params.id);
		if (!breed) {
			throw new ErrorHandler(404, `Breed with id ${req.params.id} does not exists`);
		}
		res.status(200);
		res.json(breed);
	}
	catch (error) {
		next(error)
	  }
});

// search breed by name
// @route POST api/breed/name
// @access Public
router.get('/breed/search/:name', async (req, res, next) => {
	// case insensitive search
	const nameRegex = new RegExp(req.params.name, 'i');
	try {
		const getBreedByName = await Breed.find({ name: nameRegex });
		if (!getBreedByName || getBreedByName.length === 0) {
			throw new ErrorHandler(404, `Breed with name ${req.params.name} does not exists`);
		}
		res.status(200);
		res.json({ breeds: getBreedByName });
	} catch (error) {
		next(error);
	}
});


// Create new breed
// @route POST api/add-breed
// @access Public
router.post('/breed', async (req, res, next) => {
	try {
		if (!req.body.name || !req.body.description) {
			throw new ErrorHandler(400, `Name and description are required`);
		}
	
		const breed = new Breed({
			name: req.body.name,
			description: req.body.description,
			temperament: req.body.temperament,
			origin: req.body.origin,
		});
		createBread = await breed.save();
		if (!createBread) {
			throw new ErrorHandler(400, `Breed creation failed`);
		}
		res.status = 201;
		res.json({ message: 'breed saved..!' });

	}
	catch (error) {
		next(error);
	}
});

module.exports = router;