const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const bodyParser = require('body-parser');
const { handleError } = require('./helpers/error');
const cors = require('cors');
require('dotenv').config()

// Declaring route variables
const breed = require('./routes/breed');

const app = express();

app.use(cors());

// set static folder
app.use(express.static(path.join(__dirname, 'public')));

// body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// DB config
const db = process.env.mongoURI;
const mongoOptions = {
	keepAlive: 1,
	connectTimeoutMS: 30000,
	reconnectTries: 30,
	reconnectInterval: 5000,
	useNewUrlParser: true,
	useNewUrlParser: true,
	useCreateIndex: true
}


// Connect to MongoDB
mongoose
	.connect(db, mongoOptions)
	.then(() => console.log('Database connected successfully'))
	.catch(err => {
		console.log('Could not connect to database', err);
		process.exit(1);
	}
	);

app.use('/api/', breed);

// Page not found
const pageNotFound = (req, res) => {
	res.status(404).send({ error: 'Page not found' })
}
app.use(pageNotFound)

const port = process.env.PORT || 5000;
app.use((err, req, res, next) => {
	handleError(err, res);
  });

app.listen(port, () => console.log(`Cat server running on port ${port}`));